#include "qterminallabel.h"

#include "qterminalbackend.h"
#include <QDebug>
#include <QtCore/qmath.h>

QTerminalLabel::QTerminalLabel(QString string)
{
    m_strings << string;
}

QSize QTerminalLabel::calculateSizeHint()
{
    int totlength(0);
    foreach (const QString& paragraph, m_strings)
        totlength += paragraph.length();
    totlength = qSqrt(totlength);

    QSize size(totlength, totlength);

    size = checkMinMax(size);
    return size;
}

QStringList QTerminalLabel::wrapStrings(QStringList text, int line_length)
{
    QStringList wrapped;
    foreach (const QString& paragraph, text) {
        QStringList words = paragraph.split(" ");
        QStringList line;
        QString word;

        if (!words.empty())
        {
            word = words.takeFirst();
            while (word.length() > line_length)
            {
                wrapped << word.left(line_length);
                word = word.remove(0, line_length);
            }
            line << word;
            int space_left = line_length - word.length();

            while (!words.empty())
            {
                word = words.takeFirst();
                if (space_left < word.length() + 1)
                {
                    wrapped << line.join(" ");
                    line.clear();
                    while (word.length() > line_length)
                    {
                        wrapped << word.left(line_length);
                        word = word.remove(0, line_length);
                    }
                    line << word;
                    space_left = line_length - word.length();
                }
                else
                {
                    line << word;
                    space_left -= word.length() + 1;
                }
            }
            wrapped << line.join(" ");
        }
    }

    return wrapped;
}

void QTerminalLabel::render(bool sizeChanged)
{
    qDebug() << "Rendering label" << m_sizeHint << m_rect;
    if (!m_dirty || !m_backend || !sizeChanged)
        return;

    QPoint point;

    QSize stuffing(m_margin.width() + m_padding.width() + m_borderSize.width(),
                   m_margin.height() + m_padding.height() + m_borderSize.height());
    QRect area = m_rect.adjusted(stuffing.width(), stuffing.height(), -stuffing.width(), -stuffing.height());

    m_backend->fillRect(area, ' ');

    QStringList text = wrapStrings(m_strings, area.width());

    for (int l = 0; l < text.size() && l < area.height(); l++)
    {
        const QString& line = text.at(l);
        for (int i = 0; i < line.length() && i < area.width(); i++)
        {
            if (Qt::Horizontal == m_orientation)
            {
                point.setX(area.x() + i);
                point.setY(area.y() + l);
            }
            else
            {
                point.setX(area.x() + l);
                point.setY(area.y() + i);
            }
            m_backend->putChar(point, (u_int32_t)line.at(i).unicode());
        }
    }
}

void QTerminalLabel::recalculateSizeHints()
{
    m_sizeHint = calculateSizeHint();
    qDebug() << "Recalcing label" << m_sizeHint;
    m_dirty = true;
}
