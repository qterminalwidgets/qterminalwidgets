#include "qterminaltextedit.h"

#include "qterminalbackend.h"

QTerminalTextEdit::QTerminalTextEdit()
{
}

void QTerminalTextEdit::keyEvent(const QTerminalKeyEvent &event)
{
    qDebug("textedit got a key");
    if (event.key > 0)
    {
        switch (event.key) {
        case Qt::Key_Return:
        case Qt::Key_Enter:
            m_strings << QString();
            break;
        case Qt::Key_Backspace:
            if (!m_strings.empty() && !m_strings.last().isEmpty())
                m_strings.last().chop(1);
            else if (m_strings.size() > 1)
                m_strings.removeLast();
            break;
        default:
            return;
        }
        emit updated();
    }
    if (event.ch > 0)
    {
        if (m_strings.empty())
            m_strings << QString();
        m_strings.last() += event.ch;
        emit updated();
    }
}

void QTerminalTextEdit::render(bool sizeChanged)
{
    if (!m_dirty || !m_backend)
        return;

    QPoint point;

    QSize stuffing(m_margin.width() + m_padding.width() + m_borderSize.width(),
                   m_margin.height() + m_padding.height() + m_borderSize.height());
    QRect area = m_rect.adjusted(stuffing.width(), stuffing.height(), -stuffing.width(), -stuffing.height());

    m_backend->fillRect(area, ' ');

    if (area.height() > 1) // area - scroll lines upwards when filling
    {
        QStringList text = wrapStrings(m_strings, area.width());

        int startLine = qMax(0, text.size() - area.height());
        for (int l = 0; l + startLine < text.size() && l < area.height(); l++)
        {
            const QString& line = text.at(l + startLine);
            for (int i = 0; i < line.length() && i < area.width(); i++)
            {
    //            qDebug() << "Putting char" << m_rect.x() + i << m_rect.y() << m_rect;

                if (Qt::Horizontal == m_orientation)
                {
                    point.setX(area.x() + i);
                    point.setY(area.y() + l);
                }
                else
                {
                    point.setX(area.x() + l);
                    point.setY(area.y() + i);
                }
                m_backend->putChar(point, (u_int32_t)line.at(i).unicode());
            }
        }
    }
//    else // single line - scroll text to the side while inserting text
//    {
//        QString text = m_strings.last();
//        if (Qt::Horizontal == m_orientation)
//        {
//            text = text.right(area.width());
//            for (int i = 0; i < line.length() && i < area.width(); i++)
//            point.setX(area.x() + i);
//            point.setY(area.y() + l);
//        }
//        else
//        {
//            point.setX(area.x() + l);
//            point.setY(area.y() + i);
//        }
//    }
}
