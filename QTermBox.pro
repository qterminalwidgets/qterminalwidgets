#-------------------------------------------------
#
# Project created by QtCreator 2014-02-26T00:24:45
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = QTermBox
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    qterminalwindow.cpp \
    qterminalwidget.cpp \
    qtermboxbackend.cpp \
    qncursesbackend.cpp \
    qsignalhandler/qsignalhandler.cpp \
    qterminallabel.cpp \
    qterminalcontainerwidget.cpp \
    qterminaltextedit.cpp

unix:!macx: LIBS += -ltermbox

HEADERS += \
    qterminalwindow.h \
    qterminalwidget.h \
    qtermboxbackend.h \
    qterminalbackend.h \
    qncursesbackend.h \
    qsignalhandler/qsignalhandler.h \
    qterminallabel.h \
    qterminalcontainerwidget.h \
    qterminaltextedit.h

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += ncursesw termkey
