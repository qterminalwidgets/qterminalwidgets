#include "qterminalwidget.h"

#include "qterminalbackend.h"
#include <QDebug>

QTerminalWidget::QTerminalWidget() :
    m_parent(0),
    m_backend(0),
    m_orientation(Qt::Horizontal),
    m_dirty(true),
    m_margin(QSize(0, 0)),
    m_padding(QSize(0, 0)),
    m_borderSize(QSize(1, 1)),
    m_border(HeavyBorder),
    m_verticalPolicy(Preferred),
    m_horizontalPolicy(Preferred)
{
    connect(this, &QTerminalWidget::updated, this, &QTerminalWidget::onUpdate);
}

QTerminalWidget::~QTerminalWidget()
{

}

void QTerminalWidget::setParent(QTerminalContainerWidget *parent)
{
    m_parent = parent;
}

void QTerminalWidget::onUpdate()
{
    QSize sizeHint = calculateSizeHint();

    m_dirty = true;

    if (sizeHint != m_sizeHint)
    {
        qDebug("onUpdate - no change");
        m_sizeHint = sizeHint;
        emit sizeHintChanged(); // dirty, new sizehint, size may change (see setGeometry)
    }
    else
    {
        qDebug("onUpdate - rerendering");
        render(false); // dirty, old sizehint, no change in size
        m_dirty = false;
        m_backend->present();
    }
}

QSize QTerminalWidget::checkMinMax(QSize size)
{

    size.rheight() += (m_margin.height() + m_padding.height() + m_borderSize.height()) * 2;
    size.rwidth() += (m_margin.width() + m_padding.width() + m_borderSize.width()) * 2;

    if (m_minSize.width() > 0)
        size.setWidth(qMax(size.width(), m_minSize.width()));
    if (m_minSize.height() > 0)
        size.setHeight(qMax(size.height(), m_minSize.height()));
    if (m_maxSize.width() > 0)
        size.setWidth(qMin(size.width(), m_maxSize.width()));
    if (m_maxSize.height() > 0)
        size.setHeight(qMin(size.height(), m_maxSize.height()));
    return size;
}

void QTerminalWidget::setGeometry(const QRect &rect) {
    qDebug() << rect << m_rect;
    if (rect != m_rect)
    {
        m_rect = rect;
        m_dirty = true;
        renderBorder();
        render(true); // size has changed
    }
    else if (m_dirty)
    {
        render(false); // dirty, new sizehint, but size unchanged
    }
    m_dirty = false;
}

void QTerminalWidget::renderBorder()
{
    if (!m_dirty)
        return;

    m_backend->drawBorder(m_rect, NoBorder, m_margin);

    QRect adjusted = m_rect.adjusted(m_margin.width(), m_margin.height(), -m_margin.width(), -m_margin.height());
    m_backend->drawBorder(adjusted, m_border, m_borderSize);

    adjusted.adjust(m_borderSize.width(), m_borderSize.height(), -m_borderSize.width(), -m_borderSize.height());
    m_backend->drawBorder(adjusted, NoBorder, m_padding);
}
