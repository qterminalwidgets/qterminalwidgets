#ifndef QTERMINALWIDGET_H
#define QTERMINALWIDGET_H

#include <QObject>
#include <QPoint>
#include <QRect>
//#include <QEvent>

enum QTerminalBorder {
    NoBorder,
    LightBorder,
    DoubleBorder,
    HeavyBorder,
    LightDoubleDashBorder,
    HeavyDoubleDashBorder,
    LightTripleDashBorder,
    HeavyTripleDashBorder,
    LightQuadrupleDashBorder,
    HeavyQuadrupleDashBorder,
    LightArcBorder
};

enum PolicyFlag {
    GrowFlag = 1,
    ExpandFlag = 2,
    ShrinkFlag = 4,
    IgnoreFlag = 8
};

enum Policy {
    Fixed = 0,
    Minimum = GrowFlag,
    Maximum = ShrinkFlag,
    Preferred = GrowFlag | ShrinkFlag,
    MinimumExpanding = GrowFlag | ExpandFlag,
    Expanding = GrowFlag | ShrinkFlag | ExpandFlag,
    Ignored = ShrinkFlag | GrowFlag | IgnoreFlag
};

struct QTerminalKeyEvent {
    Qt::Key key;
    quint32 ch;
    Qt::KeyboardModifiers mods;
};

struct QTerminalMouseEvent {
    Qt::Key button;
    //QEvent::Type QEvent::MouseButtonPress
    bool pressed;
    Qt::KeyboardModifiers mods;
};

class QTerminalBackend;
class QTerminalContainerWidget;

class QTerminalWidget : public QObject
{
    Q_OBJECT
public:
    QTerminalWidget();
    ~QTerminalWidget();

    void setParent(QTerminalContainerWidget * parent);
    void setBackend(QTerminalBackend * backend) { m_backend = backend; }
    virtual QTerminalContainerWidget* parent() { return 0; }
    virtual void recalculateSizeHints() = 0;

public slots:
    virtual void keyEvent(const QTerminalKeyEvent &) {}
    virtual void mouseEvent(const QTerminalMouseEvent &) {}
signals:
    void sizeHintChanged();
    void updated();
protected slots:
    void onUpdate();
    virtual QSize calculateSizeHint() = 0;
protected:
    QSize checkMinMax(QSize size);
public:
    virtual QSize sizeHint() { return m_sizeHint; }
    virtual QSize minimumSize() const { return m_minSize; }
    virtual void setMinimumSize(const QSize& size ) { m_minSize = size; }
    virtual QSize maximumSize() const { return m_maxSize; }
    virtual void setMaximumSize(const QSize& size ) { m_maxSize = size; }
    virtual Qt::Orientation orientation() const { return m_orientation; }
    virtual Policy verticalPolicy() const { return m_verticalPolicy; }
    virtual Policy horizontalPolicy() const { return m_horizontalPolicy; }
//    virtual Qt::Orientations expandingDirections() const { return m_orientation; }
    virtual void setOrientation(Qt::Orientation orientation) { m_orientation = orientation; }
    virtual void setGeometry(const QRect& rect);
    virtual QRect geometry() const { return m_rect; }
    virtual void render(bool sizeChanged) = 0;
    virtual void renderBorder();
    virtual bool dirty() { return m_dirty; }

protected:
    QTerminalContainerWidget* m_parent;
    QTerminalBackend* m_backend;
    QRect m_rect;
    QSize m_minSize, m_maxSize, m_sizeHint;
    QPoint m_cursor;
    Qt::Orientation m_orientation;
    bool m_dirty;

    QSize m_margin, m_padding, m_borderSize; // padding = inside of border, margin = outside of border
    QTerminalBorder m_border;
    Policy m_verticalPolicy, m_horizontalPolicy;
};







































//inline void QTerminalWidget::move(int ax, int ay)
//{ move(QPoint(ax, ay)); }

//inline void QTerminalWidget::resize(int w, int h)
//{ resize(QSize(w, h)); }

#endif // QTERMINALWIDGET_H

#ifndef QTERMINALWIDGET_H
#define QTERMINALWIDGET_H

#include <QObject>
#include <QRect>

//#include <QtGui/qwindowdefs.h>
//#include <QtCore/qobject.h>
//#include <QtCore/qmargins.h>
//#include <QtGui/qpaintdevice.h>
//#include <QtGui/qpalette.h>
//#include <QtGui/qfont.h>
//#include <QtGui/qfontmetrics.h>
//#include <QtGui/qfontinfo.h>
//#include <QtWidgets/qsizepolicy.h>
//#include <QtGui/qregion.h>
//#include <QtGui/qbrush.h>
//#include <QtGui/qcursor.h>
//#include <QtGui/qkeysequence.h>

//#ifdef QT_INCLUDE_COMPAT
//#include <QtGui/qevent.h>
//#endif

//QT_BEGIN_NAMESPACE


class QLayout;
//class QWSRegionManager;
//class QStyle;
//class QAction;
class QVariant;
//class QWindow;
class QActionEvent;
//class QMouseEvent;
//class QWheelEvent;
//class QHoverEvent;
class QKeyEvent;
class QFocusEvent;
class QPaintEvent;
class QMoveEvent;
class QResizeEvent;
class QCloseEvent;
class QContextMenuEvent;
class QInputMethodEvent;
class QTabletEvent;
class QDragEnterEvent;
class QDragMoveEvent;
class QDragLeaveEvent;
class QDropEvent;
class QShowEvent;
class QHideEvent;
//class QIcon;
class QBackingStore;
//class QPlatformWindow;
//class QLocale;
//class QGraphicsProxyWidget;
//class QGraphicsEffect;
//class QRasterWindowSurface;
class QUnifiedToolbarSurface;
//class QPixmap;

class QTerminalWidgetData
{
public:
//    WId winid;
    uint widget_attributes;
    Qt::WindowFlags window_flags;
    uint window_state : 4;
    uint focus_policy : 4;
    uint sizehint_forced :1;
    uint is_closing :1;
    uint in_show : 1;
    uint in_set_window_state : 1;
    mutable uint fstrut_dirty : 1;
    uint context_menu_policy : 3;
    uint window_modality : 2;
    uint in_destructor : 1;
    uint unused : 13;
    QRect crect;
//    mutable QPalette pal;
//    QFont fnt;
    QRect wrect;
};

//class QTerminalWidgetPrivate;

class Q_WIDGETS_EXPORT QTerminalWidget : public QObject//, public QPaintDevice
{
    Q_OBJECT
//    Q_DECLARE_PRIVATE(QTerminalWidget)

    Q_PROPERTY(bool modal READ isModal)
    Q_PROPERTY(Qt::WindowModality windowModality READ windowModality WRITE setWindowModality)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled)
    Q_PROPERTY(QRect geometry READ geometry WRITE setGeometry)
    Q_PROPERTY(QRect frameGeometry READ frameGeometry)
    Q_PROPERTY(QRect normalGeometry READ normalGeometry)
    Q_PROPERTY(int x READ x)
    Q_PROPERTY(int y READ y)
    Q_PROPERTY(QPoint pos READ pos WRITE move DESIGNABLE false STORED false)
    Q_PROPERTY(QSize frameSize READ frameSize)
    Q_PROPERTY(QSize size READ size WRITE resize DESIGNABLE false STORED false)
    Q_PROPERTY(int width READ width)
    Q_PROPERTY(int height READ height)
    Q_PROPERTY(QRect rect READ rect)
    Q_PROPERTY(QRect childrenRect READ childrenRect)
//    Q_PROPERTY(QRegion childrenRegion READ childrenRegion)
//    Q_PROPERTY(QSizePolicy sizePolicy READ sizePolicy WRITE setSizePolicy)
    Q_PROPERTY(QSize minimumSize READ minimumSize WRITE setMinimumSize)
    Q_PROPERTY(QSize maximumSize READ maximumSize WRITE setMaximumSize)
    Q_PROPERTY(int minimumWidth READ minimumWidth WRITE setMinimumWidth STORED false DESIGNABLE false)
    Q_PROPERTY(int minimumHeight READ minimumHeight WRITE setMinimumHeight STORED false DESIGNABLE false)
    Q_PROPERTY(int maximumWidth READ maximumWidth WRITE setMaximumWidth STORED false DESIGNABLE false)
    Q_PROPERTY(int maximumHeight READ maximumHeight WRITE setMaximumHeight STORED false DESIGNABLE false)
    Q_PROPERTY(QSize sizeIncrement READ sizeIncrement WRITE setSizeIncrement)
    Q_PROPERTY(QSize baseSize READ baseSize WRITE setBaseSize)
//    Q_PROPERTY(QPalette palette READ palette WRITE setPalette)
//    Q_PROPERTY(QFont font READ font WRITE setFont)
//#ifndef QT_NO_CURSOR
//    Q_PROPERTY(QCursor cursor READ cursor WRITE setCursor RESET unsetCursor)
//#endif
    Q_PROPERTY(bool mouseTracking READ hasMouseTracking WRITE setMouseTracking)
    Q_PROPERTY(bool isActiveWindow READ isActiveWindow)
    Q_PROPERTY(Qt::FocusPolicy focusPolicy READ focusPolicy WRITE setFocusPolicy)
    Q_PROPERTY(bool focus READ hasFocus)
    Q_PROPERTY(Qt::ContextMenuPolicy contextMenuPolicy READ contextMenuPolicy WRITE setContextMenuPolicy)
    Q_PROPERTY(bool updatesEnabled READ updatesEnabled WRITE setUpdatesEnabled DESIGNABLE false)
    Q_PROPERTY(bool visible READ isVisible WRITE setVisible DESIGNABLE false)
    Q_PROPERTY(bool minimized READ isMinimized)
    Q_PROPERTY(bool maximized READ isMaximized)
    Q_PROPERTY(bool fullScreen READ isFullScreen)
    Q_PROPERTY(QSize sizeHint READ sizeHint)
    Q_PROPERTY(QSize minimumSizeHint READ minimumSizeHint)
//    Q_PROPERTY(bool acceptDrops READ acceptDrops WRITE setAcceptDrops)
    Q_PROPERTY(QString windowTitle READ windowTitle WRITE setWindowTitle NOTIFY windowTitleChanged DESIGNABLE isWindow)
//    Q_PROPERTY(QIcon windowIcon READ windowIcon WRITE setWindowIcon NOTIFY windowIconChanged DESIGNABLE isWindow)
    Q_PROPERTY(QString windowIconText READ windowIconText WRITE setWindowIconText NOTIFY windowIconTextChanged DESIGNABLE isWindow)
    Q_PROPERTY(double windowOpacity READ windowOpacity WRITE setWindowOpacity DESIGNABLE isWindow)
    Q_PROPERTY(bool windowModified READ isWindowModified WRITE setWindowModified DESIGNABLE isWindow)
//#ifndef QT_NO_TOOLTIP
//    Q_PROPERTY(QString toolTip READ toolTip WRITE setToolTip)
//    Q_PROPERTY(int toolTipDuration READ toolTipDuration WRITE setToolTipDuration)
//#endif
//#ifndef QT_NO_STATUSTIP
//    Q_PROPERTY(QString statusTip READ statusTip WRITE setStatusTip)
//#endif
//#ifndef QT_NO_WHATSTHIS
//    Q_PROPERTY(QString whatsThis READ whatsThis WRITE setWhatsThis)
//#endif
//#ifndef QT_NO_ACCESSIBILITY
//    Q_PROPERTY(QString accessibleName READ accessibleName WRITE setAccessibleName)
//    Q_PROPERTY(QString accessibleDescription READ accessibleDescription WRITE setAccessibleDescription)
//#endif
    Q_PROPERTY(Qt::LayoutDirection layoutDirection READ layoutDirection WRITE setLayoutDirection RESET unsetLayoutDirection)
    QDOC_PROPERTY(Qt::WindowFlags windowFlags READ windowFlags WRITE setWindowFlags)
    Q_PROPERTY(bool autoFillBackground READ autoFillBackground WRITE setAutoFillBackground)
//#ifndef QT_NO_STYLE_STYLESHEET
//    Q_PROPERTY(QString styleSheet READ styleSheet WRITE setStyleSheet)
//#endif
//    Q_PROPERTY(QLocale locale READ locale WRITE setLocale RESET unsetLocale)
    Q_PROPERTY(QString windowFilePath READ windowFilePath WRITE setWindowFilePath DESIGNABLE isWindow)
    Q_PROPERTY(Qt::InputMethodHints inputMethodHints READ inputMethodHints WRITE setInputMethodHints)

public:
    enum RenderFlag {
        DrawWindowBackground = 0x1,
        DrawChildren = 0x2,
        IgnoreMask = 0x4
    };
    Q_DECLARE_FLAGS(RenderFlags, RenderFlag)

    explicit QTerminalWidget(QTerminalWidget* parent = 0, Qt::WindowFlags f = 0);
    ~QTerminalWidget();

    int devType() const;

//    WId winId() const;
//    void createWinId(); // internal, going away
//    inline WId internalWinId() const { return data->winid; }
//    WId effectiveWinId() const;

    // GUI style setting
//    QStyle *style() const;
//    void setStyle(QStyle *);
    // Widget types and states

    bool isTopLevel() const;
    bool isWindow() const;

    bool isModal() const;
    Qt::WindowModality windowModality() const;
    void setWindowModality(Qt::WindowModality windowModality);

    bool isEnabled() const;
    bool isEnabledTo(const QTerminalWidget *) const;
    bool isEnabledToTLW() const;

public Q_SLOTS:
    void setEnabled(bool);
    void setDisabled(bool);
    void setWindowModified(bool);

    // Widget coordinates

public:
    QRect frameGeometry() const;
    const QRect &geometry() const;
    QRect normalGeometry() const;

    int x() const;
    int y() const;
    QPoint pos() const;
    QSize frameSize() const;
    QSize size() const;
    inline int width() const;
    inline int height() const;
    inline QRect rect() const;
    QRect childrenRect() const;
//    QRegion childrenRegion() const;

    QSize minimumSize() const;
    QSize maximumSize() const;
    int minimumWidth() const;
    int minimumHeight() const;
    int maximumWidth() const;
    int maximumHeight() const;
    void setMinimumSize(const QSize &);
    void setMinimumSize(int minw, int minh);
    void setMaximumSize(const QSize &);
    void setMaximumSize(int maxw, int maxh);
    void setMinimumWidth(int minw);
    void setMinimumHeight(int minh);
    void setMaximumWidth(int maxw);
    void setMaximumHeight(int maxh);

#ifdef Q_QDOC
    void setupUi(QTerminalWidget *widget);
#endif

    QSize sizeIncrement() const;
    void setSizeIncrement(const QSize &);
    void setSizeIncrement(int w, int h);
    QSize baseSize() const;
    void setBaseSize(const QSize &);
    void setBaseSize(int basew, int baseh);

    void setFixedSize(const QSize &);
    void setFixedSize(int w, int h);
    void setFixedWidth(int w);
    void setFixedHeight(int h);

    // Widget coordinate mapping

    QPoint mapToGlobal(const QPoint &) const;
    QPoint mapFromGlobal(const QPoint &) const;
    QPoint mapToParent(const QPoint &) const;
    QPoint mapFromParent(const QPoint &) const;
    QPoint mapTo(const QTerminalWidget *, const QPoint &) const;
    QPoint mapFrom(const QTerminalWidget *, const QPoint &) const;

    QTerminalWidget *window() const;
    QTerminalWidget *nativeParentWidget() const;
    inline QTerminalWidget *topLevelWidget() const { return window(); }

    // Widget appearance functions
    const QPalette &palette() const;
    void setPalette(const QPalette &);

//    void setBackgroundRole(QPalette::ColorRole);
//    QPalette::ColorRole backgroundRole() const;

//    void setForegroundRole(QPalette::ColorRole);
//    QPalette::ColorRole foregroundRole() const;

//    const QFont &font() const;
//    void setFont(const QFont &);
//    QFontMetrics fontMetrics() const;
//    QFontInfo fontInfo() const;

#ifndef QT_NO_CURSOR
    QCursor cursor() const;
    void setCursor(const QCursor &);
    void unsetCursor();
#endif

    void setMouseTracking(bool enable);
    bool hasMouseTracking() const;
    bool underMouse() const;

//    void setMask(const QBitmap &);
//    void setMask(const QRegion &);
//    QRegion mask() const;
//    void clearMask();

//    void render(QPaintDevice *target, const QPoint &targetOffset = QPoint(),
//                const QRegion &sourceRegion = QRegion(),
//                RenderFlags renderFlags = RenderFlags(DrawWindowBackground | DrawChildren));

//    void render(QPainter *painter, const QPoint &targetOffset = QPoint(),
//                const QRegion &sourceRegion = QRegion(),
//                RenderFlags renderFlags = RenderFlags(DrawWindowBackground | DrawChildren));

//    Q_INVOKABLE QPixmap grab(const QRect &rectangle = QRect(QPoint(0, 0), QSize(-1, -1)));

//#ifndef QT_NO_GRAPHICSEFFECT
//    QGraphicsEffect *graphicsEffect() const;
//    void setGraphicsEffect(QGraphicsEffect *effect);
//#endif //QT_NO_GRAPHICSEFFECT

//#ifndef QT_NO_GESTURES
//    void grabGesture(Qt::GestureType type, Qt::GestureFlags flags = Qt::GestureFlags());
//    void ungrabGesture(Qt::GestureType type);
//#endif

public Q_SLOTS:
    void setWindowTitle(const QString &);
#ifndef QT_NO_STYLE_STYLESHEET
    void setStyleSheet(const QString& styleSheet);
#endif
public:
#ifndef QT_NO_STYLE_STYLESHEET
    QString styleSheet() const;
#endif
    QString windowTitle() const;
    void setWindowIcon(const QIcon &icon);
    QIcon windowIcon() const;
    void setWindowIconText(const QString &);
    QString windowIconText() const;
    void setWindowRole(const QString &);
    QString windowRole() const;
    void setWindowFilePath(const QString &filePath);
    QString windowFilePath() const;

    void setWindowOpacity(qreal level);
    qreal windowOpacity() const;

    bool isWindowModified() const;
//#ifndef QT_NO_TOOLTIP
//    void setToolTip(const QString &);
//    QString toolTip() const;
//    void setToolTipDuration(int msec);
//    int toolTipDuration() const;
//#endif
//#ifndef QT_NO_STATUSTIP
//    void setStatusTip(const QString &);
//    QString statusTip() const;
//#endif
//#ifndef QT_NO_WHATSTHIS
//    void setWhatsThis(const QString &);
//    QString whatsThis() const;
//#endif
//#ifndef QT_NO_ACCESSIBILITY
//    QString accessibleName() const;
//    void setAccessibleName(const QString &name);
//    QString accessibleDescription() const;
//    void setAccessibleDescription(const QString &description);
//#endif

    void setLayoutDirection(Qt::LayoutDirection direction);
    Qt::LayoutDirection layoutDirection() const;
    void unsetLayoutDirection();

    void setLocale(const QLocale &locale);
    QLocale locale() const;
    void unsetLocale();

    inline bool isRightToLeft() const { return layoutDirection() == Qt::RightToLeft; }
    inline bool isLeftToRight() const { return layoutDirection() == Qt::LeftToRight; }

public Q_SLOTS:
    inline void setFocus() { setFocus(Qt::OtherFocusReason); }

public:
    bool isActiveWindow() const;
    void activateWindow();
    void clearFocus();

    void setFocus(Qt::FocusReason reason);
    Qt::FocusPolicy focusPolicy() const;
    void setFocusPolicy(Qt::FocusPolicy policy);
    bool hasFocus() const;
    static void setTabOrder(QTerminalWidget *, QTerminalWidget *);
    void setFocusProxy(QTerminalWidget *);
    QTerminalWidget *focusProxy() const;
    Qt::ContextMenuPolicy contextMenuPolicy() const;
    void setContextMenuPolicy(Qt::ContextMenuPolicy policy);

    // Grab functions
    void grabMouse();
#ifndef QT_NO_CURSOR
    void grabMouse(const QCursor &);
#endif
    void releaseMouse();
    void grabKeyboard();
    void releaseKeyboard();
#ifndef QT_NO_SHORTCUT
    int grabShortcut(const QKeySequence &key, Qt::ShortcutContext context = Qt::WindowShortcut);
    void releaseShortcut(int id);
    void setShortcutEnabled(int id, bool enable = true);
    void setShortcutAutoRepeat(int id, bool enable = true);
#endif
    static QTerminalWidget *mouseGrabber();
    static QTerminalWidget *keyboardGrabber();

    // Update/refresh functions
    inline bool updatesEnabled() const;
    void setUpdatesEnabled(bool enable);

//#ifndef QT_NO_GRAPHICSVIEW
//    QGraphicsProxyWidget *graphicsProxyWidget() const;
//#endif

public Q_SLOTS:
    void update();
    void repaint();

public:
    inline void update(int x, int y, int w, int h);
    void update(const QRect&);
//    void update(const QRegion&);

    void repaint(int x, int y, int w, int h);
    void repaint(const QRect &);
//    void repaint(const QRegion &);

public Q_SLOTS:
    // Widget management functions

    virtual void setVisible(bool visible);
    void setHidden(bool hidden);
    void show();
    void hide();

    void showMinimized();
    void showMaximized();
    void showFullScreen();
    void showNormal();

    bool close();
    void raise();
    void lower();

public:
    void stackUnder(QTerminalWidget*);
    void move(int x, int y);
    void move(const QPoint &);
    void resize(int w, int h);
    void resize(const QSize &);
    inline void setGeometry(int x, int y, int w, int h);
    void setGeometry(const QRect &);
    QByteArray saveGeometry() const;
    bool restoreGeometry(const QByteArray &geometry);
    void adjustSize();
    bool isVisible() const;
    bool isVisibleTo(const QTerminalWidget *) const;
    inline bool isHidden() const;

    bool isMinimized() const;
    bool isMaximized() const;
    bool isFullScreen() const;

    Qt::WindowStates windowState() const;
    void setWindowState(Qt::WindowStates state);
    void overrideWindowState(Qt::WindowStates state);

    virtual QSize sizeHint() const;
    virtual QSize minimumSizeHint() const;

//    QSizePolicy sizePolicy() const;
//    void setSizePolicy(QSizePolicy);
//    inline void setSizePolicy(QSizePolicy::Policy horizontal, QSizePolicy::Policy vertical);
    virtual int heightForWidth(int) const;
    virtual bool hasHeightForWidth() const;

//    QRegion visibleRegion() const;

    void setContentsMargins(int left, int top, int right, int bottom);
    void setContentsMargins(const QMargins &margins);
    void getContentsMargins(int *left, int *top, int *right, int *bottom) const;
    QMargins contentsMargins() const;

    QRect contentsRect() const;

public:
    QLayout *layout() const;
    void setLayout(QLayout *);
    void updateGeometry();

    void setParent(QTerminalWidget *parent);
    void setParent(QTerminalWidget *parent, Qt::WindowFlags f);

    void scroll(int dx, int dy);
    void scroll(int dx, int dy, const QRect&);

    // Misc. functions

    QTerminalWidget *focusWidget() const;
    QTerminalWidget *nextInFocusChain() const;
    QTerminalWidget *previousInFocusChain() const;

    // drag and drop
//    bool acceptDrops() const;
//    void setAcceptDrops(bool on);

//#ifndef QT_NO_ACTION
//    //actions
//    void addAction(QAction *action);
//    void addActions(QList<QAction*> actions);
//    void insertAction(QAction *before, QAction *action);
//    void insertActions(QAction *before, QList<QAction*> actions);
//    void removeAction(QAction *action);
//    QList<QAction*> actions() const;
//#endif

    QTerminalWidget *parentWidget() const;

    void setWindowFlags(Qt::WindowFlags type);
    inline Qt::WindowFlags windowFlags() const;
    void overrideWindowFlags(Qt::WindowFlags type);

    inline Qt::WindowType windowType() const;

//    static QTerminalWidget *find(WId);
    inline QTerminalWidget *childAt(int x, int y) const;
    QTerminalWidget *childAt(const QPoint &p) const;

//#if defined(Q_WS_X11)
//    const QX11Info &x11Info() const;
//    Qt::HANDLE x11PictureHandle() const;
//#endif

//#if defined(Q_WS_MAC)
//    Qt::HANDLE macQDHandle() const;
//    Qt::HANDLE macCGHandle() const;
//#endif

    void setAttribute(Qt::WidgetAttribute, bool on = true);
    inline bool testAttribute(Qt::WidgetAttribute) const;

//    QPaintEngine *paintEngine() const;

    void ensurePolished() const;

    bool isAncestorOf(const QTerminalWidget *child) const;

#ifdef QT_KEYPAD_NAVIGATION
    bool hasEditFocus() const;
    void setEditFocus(bool on);
#endif

    bool autoFillBackground() const;
    void setAutoFillBackground(bool enabled);

    QBackingStore *backingStore() const;

//    QWindow *windowHandle() const;

//    static QTerminalWidget *createWindowContainer(QWindow *window, QTerminalWidget *parent=0, Qt::WindowFlags flags=0);

    friend class QDesktopScreenWidget;

Q_SIGNALS:
    void windowTitleChanged(const QString &title);
//    void windowIconChanged(const QIcon &icon);
    void windowIconTextChanged(const QString &iconText);
    void customContextMenuRequested(const QPoint &pos);

protected:
    // Event handlers
    bool event(QEvent *);
//    virtual void mousePressEvent(QMouseEvent *);
//    virtual void mouseReleaseEvent(QMouseEvent *);
//    virtual void mouseDoubleClickEvent(QMouseEvent *);
//    virtual void mouseMoveEvent(QMouseEvent *);
//#ifndef QT_NO_WHEELEVENT
//    virtual void wheelEvent(QWheelEvent *);
//#endif
//    virtual void keyPressEvent(QKeyEvent *);
//    virtual void keyReleaseEvent(QKeyEvent *);
//    virtual void focusInEvent(QFocusEvent *);
//    virtual void focusOutEvent(QFocusEvent *);
//    virtual void enterEvent(QEvent *);
//    virtual void leaveEvent(QEvent *);
//    virtual void paintEvent(QPaintEvent *);
//    virtual void moveEvent(QMoveEvent *);
//    virtual void resizeEvent(QResizeEvent *);
//    virtual void closeEvent(QCloseEvent *);
//#ifndef QT_NO_CONTEXTMENU
//    virtual void contextMenuEvent(QContextMenuEvent *);
//#endif
//#ifndef QT_NO_TABLETEVENT
//    virtual void tabletEvent(QTabletEvent *);
//#endif
//#ifndef QT_NO_ACTION
//    virtual void actionEvent(QActionEvent *);
//#endif

//#ifndef QT_NO_DRAGANDDROP
//    virtual void dragEnterEvent(QDragEnterEvent *);
//    virtual void dragMoveEvent(QDragMoveEvent *);
//    virtual void dragLeaveEvent(QDragLeaveEvent *);
//    virtual void dropEvent(QDropEvent *);
//#endif

    virtual void showEvent(QShowEvent *);
    virtual void hideEvent(QHideEvent *);
    virtual bool nativeEvent(const QByteArray &eventType, void *message, long *result);

    // Misc. protected functions
    virtual void changeEvent(QEvent *);

//    int metric(PaintDeviceMetric) const;
//    void initPainter(QPainter *painter) const;
//    QPaintDevice *redirected(QPoint *offset) const;
//    QPainter *sharedPainter() const;

    virtual void inputMethodEvent(QInputMethodEvent *);
public:
    virtual QVariant inputMethodQuery(Qt::InputMethodQuery) const;

    Qt::InputMethodHints inputMethodHints() const;
    void setInputMethodHints(Qt::InputMethodHints hints);

protected Q_SLOTS:
    void updateMicroFocus();
protected:

//    void create(WId = 0, bool initializeWindow = true,
//                         bool destroyOldWindow = true);
    void destroy(bool destroyWindow = true,
                 bool destroySubWindows = true);

    virtual bool focusNextPrevChild(bool next);
    inline bool focusNextChild() { return focusNextPrevChild(true); }
    inline bool focusPreviousChild() { return focusNextPrevChild(false); }

protected:
//    QTerminalWidget(QTerminalWidgetPrivate &d, QTerminalWidget* parent, Qt::WindowFlags f);
private:
    void setBackingStore(QBackingStore *store);

    bool testAttribute_helper(Qt::WidgetAttribute) const;

    QLayout *takeLayout();

//    friend class QBackingStoreDevice;
//    friend class QTerminalWidgetBackingStore;
//    friend class QApplication;
//    friend class QApplicationPrivate;
//    friend class QGuiApplication;
//    friend class QGuiApplicationPrivate;
//    friend class QBaseApplication;
//    friend class QPainter;
//    friend class QPainterPrivate;
//    friend class QPixmap; // for QPixmap::fill()
//    friend class QFontMetrics;
//    friend class QFontInfo;
//    friend class QETWidget;
//    friend class QLayout;
//    friend class QTerminalWidgetItem;
//    friend class QTerminalWidgetItemV2;
//    friend class QGLContext;
//    friend class QGLWidget;
//    friend class QGLWindowSurface;
//    friend class QX11PaintEngine;
//    friend class QWin32PaintEngine;
//    friend class QShortcutPrivate;
//    friend class QWindowSurface;
//    friend class QGraphicsProxyWidget;
//    friend class QGraphicsProxyWidgetPrivate;
//    friend class QStyleSheetStyle;
//    friend struct QTerminalWidgetExceptionCleaner;
//    friend class QTerminalWidgetWindow;
//    friend class QAccessibleWidget;
//    friend class QAccessibleTable;
//#ifndef QT_NO_GESTURES
//    friend class QGestureManager;
//    friend class QWinNativePanGestureRecognizer;
//#endif // QT_NO_GESTURES
    friend class QTerminalWidgetEffectSourcePrivate;

//#ifdef Q_OS_MAC
//    friend bool qt_mac_is_metal(const QTerminalWidget *w);
//#endif
    friend Q_WIDGETS_EXPORT QTerminalWidgetData *qt_QTerminalWidget_data(QTerminalWidget *widget);
//    friend Q_WIDGETS_EXPORT QTerminalWidgetPrivate *qt_widget_private(QTerminalWidget *widget);

private:
    Q_DISABLE_COPY(QTerminalWidget)
//    Q_PRIVATE_SLOT(d_func(), void _q_showIfNotHidden())

    QTerminalWidgetData *data;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QTerminalWidget::RenderFlags)

#ifndef Q_QDOC
template <> inline QTerminalWidget *qobject_cast<QTerminalWidget*>(QObject *o)
{
    if (!o || !o->isWidgetType()) return 0;
    return static_cast<QTerminalWidget*>(o);
}
template <> inline const QTerminalWidget *qobject_cast<const QTerminalWidget*>(const QObject *o)
{
    if (!o || !o->isWidgetType()) return 0;
    return static_cast<const QTerminalWidget*>(o);
}
#endif // !Q_QDOC

inline QTerminalWidget *QTerminalWidget::childAt(int ax, int ay) const
{ return childAt(QPoint(ax, ay)); }

inline Qt::WindowType QTerminalWidget::windowType() const
{ return static_cast<Qt::WindowType>(int(data->window_flags & Qt::WindowType_Mask)); }
inline Qt::WindowFlags QTerminalWidget::windowFlags() const
{ return data->window_flags; }

inline bool QTerminalWidget::isTopLevel() const
{ return (windowType() & Qt::Window); }

inline bool QTerminalWidget::isWindow() const
{ return (windowType() & Qt::Window); }

inline bool QTerminalWidget::isEnabled() const
{ return !testAttribute(Qt::WA_Disabled); }

inline bool QTerminalWidget::isModal() const
{ return data->window_modality != Qt::NonModal; }

inline bool QTerminalWidget::isEnabledToTLW() const
{ return isEnabled(); }

inline int QTerminalWidget::minimumWidth() const
{ return minimumSize().width(); }

inline int QTerminalWidget::minimumHeight() const
{ return minimumSize().height(); }

inline int QTerminalWidget::maximumWidth() const
{ return maximumSize().width(); }

inline int QTerminalWidget::maximumHeight() const
{ return maximumSize().height(); }

inline void QTerminalWidget::setMinimumSize(const QSize &s)
{ setMinimumSize(s.width(),s.height()); }

inline void QTerminalWidget::setMaximumSize(const QSize &s)
{ setMaximumSize(s.width(),s.height()); }

inline void QTerminalWidget::setSizeIncrement(const QSize &s)
{ setSizeIncrement(s.width(),s.height()); }

inline void QTerminalWidget::setBaseSize(const QSize &s)
{ setBaseSize(s.width(),s.height()); }

//inline const QFont &QTerminalWidget::font() const
//{ return data->fnt; }

//inline QFontMetrics QTerminalWidget::fontMetrics() const
//{ return QFontMetrics(data->fnt); }

//inline QFontInfo QTerminalWidget::fontInfo() const
//{ return QFontInfo(data->fnt); }

//inline void QTerminalWidget::setMouseTracking(bool enable)
//{ setAttribute(Qt::WA_MouseTracking, enable); }

//inline bool QTerminalWidget::hasMouseTracking() const
//{ return testAttribute(Qt::WA_MouseTracking); }

//inline bool QTerminalWidget::underMouse() const
//{ return testAttribute(Qt::WA_UnderMouse); }

inline bool QTerminalWidget::updatesEnabled() const
{ return !testAttribute(Qt::WA_UpdatesDisabled); }

inline void QTerminalWidget::update(int ax, int ay, int aw, int ah)
{ update(QRect(ax, ay, aw, ah)); }

inline bool QTerminalWidget::isVisible() const
{ return testAttribute(Qt::WA_WState_Visible); }

inline bool QTerminalWidget::isHidden() const
{ return testAttribute(Qt::WA_WState_Hidden); }

inline void QTerminalWidget::move(int ax, int ay)
{ move(QPoint(ax, ay)); }

inline void QTerminalWidget::resize(int w, int h)
{ resize(QSize(w, h)); }

inline void QTerminalWidget::setGeometry(int ax, int ay, int aw, int ah)
{ setGeometry(QRect(ax, ay, aw, ah)); }

inline QRect QTerminalWidget::rect() const
{ return QRect(0,0,data->crect.width(),data->crect.height()); }

inline const QRect &QTerminalWidget::geometry() const
{ return data->crect; }

inline QSize QTerminalWidget::size() const
{ return data->crect.size(); }

inline int QTerminalWidget::width() const
{ return data->crect.width(); }

inline int QTerminalWidget::height() const
{ return data->crect.height(); }

inline QTerminalWidget *QTerminalWidget::parentWidget() const
{ return static_cast<QTerminalWidget *>(QObject::parent()); }

//inline void QTerminalWidget::setSizePolicy(QSizePolicy::Policy hor, QSizePolicy::Policy ver)
//{ setSizePolicy(QSizePolicy(hor, ver)); }

inline bool QTerminalWidget::testAttribute(Qt::WidgetAttribute attribute) const
{
    if (attribute < int(8*sizeof(uint)))
        return data->widget_attributes & (1<<attribute);
    return testAttribute_helper(attribute);
}


#define QTerminalWidgetSIZE_MAX ((1<<24)-1)

QT_END_NAMESPACE

#endif // QTERMINALWIDGET_H
