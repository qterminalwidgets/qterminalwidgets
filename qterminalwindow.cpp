#include "qterminalwindow.h"
#include "qtermboxbackend.h"
#include "qncursesbackend.h"

#include <QDebug>
#include <QHash>

QTerminalWindow::QTerminalWindow(QTerminalBackend * backend)
{
    setBackend(backend ? backend : new QNcursesBackend());
    qDebug() << "window backend " << m_backend;
    connect(m_backend, &QTerminalBackend::resizeEvent, this, &QTerminalWindow::resize);
    qRegisterMetaType<QTerminalKeyEvent>("QTerminalKeyEvent");
    connect(m_backend, &QTerminalBackend::keyEvent, this, &QTerminalWindow::keyEvent);
    connect(m_backend, &QTerminalBackend::mouseEvent, this, &QTerminalWindow::mouseEvent);
}

void QTerminalWindow::keyEvent(const QTerminalKeyEvent &event)
{
    qDebug() << "Got a key" << event.ch;
    //    hide();
}

void QTerminalWindow::mouseEvent(const QTerminalMouseEvent &event)
{

}

void QTerminalWindow::resize(const QSize &size)
{
    qDebug() << "Got a resize" << size;
    QRect rect;
    rect.setWidth(size.width());
    rect.setHeight(size.height());
    qDebug() << "Sertting resct" << rect;
    setGeometry(rect);
    m_backend->present();
}

void QTerminalWindow::show()
{
    recalculateSizeHints();
    m_backend->show();
//    render();
}

void QTerminalWindow::hide()
{
    m_backend->hide();
}

void QTerminalWindow::setFocus(QTerminalWidget *widget)
{
    connect(m_backend, &QTerminalBackend::keyEvent, widget, &QTerminalWidget::keyEvent);
}

QTerminalWindow::~QTerminalWindow()
{
    hide();
}
