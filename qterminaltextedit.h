#ifndef QTERMINALTEXTEDIT_H
#define QTERMINALTEXTEDIT_H

#include "qterminallabel.h"

class QTerminalTextEdit : public QTerminalLabel
{
    Q_OBJECT
public:
    QTerminalTextEdit();
    virtual void render(bool sizeChanged);

public slots:
    void keyEvent(const QTerminalKeyEvent &event);
};

#endif // QTERMINALTEXTEDIT_H
