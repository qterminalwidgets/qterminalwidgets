#ifndef QNCURSESBACKEND_H
#define QNCURSESBACKEND_H

#include "qterminalbackend.h"
#include "qsignalhandler/qsignalhandler.h"

#include <termkey.h>

#include <QThread>

class QNcursesListener : public QThread
{
    Q_OBJECT
public:
    QNcursesListener();
    void run();
    void stop();
private:
    void on_key(TermKey *tk, TermKeyKey *key, QHash<uint, Qt::Key> *keyMap);
signals:
    void resizeEvent(QSize rect);
    void keyEvent(QTerminalKeyEvent event);
    void mouseEvent(QTerminalMouseEvent event);
private:
    bool continueRun;
    int lastMouseClick;
    qint64 lastMouseEventTime;
};

class QNcursesBackend : public QTerminalBackend
{
    Q_OBJECT
public:
    QNcursesBackend();
    bool present();
    bool show();
    bool hide();
    bool clearScreen();

    QSize size();
    void putChar(QPoint point, quint32 ch);
    void drawBorder(QRect rect, QTerminalBorder border, QSize size);
    void fillRect(QRect rect, qint32 ch);

private slots:
    void onResize(QSignalHandler::PosixSignal signal);

private:
    QNcursesListener *m_listener;
    QSignalHandler *m_signalHandler;
    bool m_visible;
};

#endif // QNCURSESBACKEND_H
