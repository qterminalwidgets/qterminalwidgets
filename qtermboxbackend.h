#ifndef QTERMBOXBACKEND_H
#define QTERMBOXBACKEND_H

#include "qterminalbackend.h"

#include <QThread>

class QTermBoxListener : public QThread
{
    Q_OBJECT
public:
    void run();
    void shutDown();
signals:
    void resizeEvent(QSize rect);
    void keyEvent(QTerminalKeyEvent event);
};

class QTermBoxBackend : public QTerminalBackend
{
    Q_OBJECT
public:
    QTermBoxBackend();
    bool present();
    bool show();
    bool hide();
    bool clearScreen();

    QSize size();

    void putChar(QPoint point, quint32 ch);
private:
    QTermBoxListener *m_listener;
    bool m_visible;
};

#endif // QTERMBOXBACKEND_H
