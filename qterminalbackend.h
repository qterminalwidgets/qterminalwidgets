#ifndef QTERMINALBACKEND_H
#define QTERMINALBACKEND_H

#include <QObject>
#include <QSize>
#include <QPoint>

#include "qterminalwidget.h"

class QTerminalBackend : public QObject
{
    Q_OBJECT
public:
    virtual bool present() = 0;
    virtual bool show() = 0;
    virtual bool hide() = 0;
    virtual bool clearScreen() = 0;

    virtual QSize size() = 0;

    virtual void putChar(QPoint point, quint32 ch) = 0;
    virtual void drawBorder(QRect rect, QTerminalBorder border, QSize size) = 0;
    virtual void fillRect(QRect rect, qint32 ch) = 0;
signals:
    void resizeEvent(QSize rect);
    void keyEvent(QTerminalKeyEvent event);
    void mouseEvent(QTerminalMouseEvent event);
};

#endif // QTERMINALBACKEND_H
