#ifndef QTERMINALCONTAINERWIDGET_H
#define QTERMINALCONTAINERWIDGET_H

#include "qterminalwidget.h"

class QTerminalContainerWidget : public QTerminalWidget
{
    Q_OBJECT
public:
    QTerminalContainerWidget();
    ~QTerminalContainerWidget();

    void addChild(QTerminalWidget* widget);

    virtual QSize calculateSizeHint();
    virtual void setGeometry(const QRect& rect);
    virtual void render(bool);
    void recalculateSizeHints();

public slots:
    void childSizeHintChanged();

protected:
    QList<QTerminalWidget*> m_children;
};

#endif // QTERMINALCONTAINERWIDGET_H
