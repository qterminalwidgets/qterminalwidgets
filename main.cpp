#include <QCoreApplication>

#include "qterminalwindow.h"
#include "qterminallabel.h"
#include "qterminaltextedit.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTerminalWindow qw;

//    qw.setOrientation(Qt::Vertical);

    QTerminalLabel l1("toptop2");
    QTerminalLabel l2("middlemiddle");
    QTerminalTextEdit l3;
//    QTerminalLabel l3("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.");
//    l1.setMinimumSize(QSize(2, 2));
//    l2.setMaximumSize(QSize(0, 4));
//    l2.setMinimumSize(QSize(0, 4));
//    l3.setMaximumSize(QSize(0, 1));

    qw.addChild(&l1);
    qw.addChild(&l2);
    qw.addChild(&l3);

    qw.setFocus(&l3);

    qw.show();

    return a.exec();
}
