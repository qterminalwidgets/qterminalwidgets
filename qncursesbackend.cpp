#include "qncursesbackend.h"

#include <ncurses.h>
#include <poll.h>
#include <QDebug>
#include <QDateTime>

QNcursesBackend::QNcursesBackend()
{
    m_listener = new QNcursesListener();

    connect(m_listener, &QNcursesListener::resizeEvent, this, &QTerminalBackend::resizeEvent);
    connect(m_listener, &QNcursesListener::keyEvent, this, &QTerminalBackend::keyEvent);
    connect(m_listener, &QNcursesListener::mouseEvent, this, &QTerminalBackend::mouseEvent);

    m_signalHandler = new QSignalHandler();

    m_signalHandler->listenToSignal(QSignalHandler::Sigwinch);

    connect(m_signalHandler, &QSignalHandler::signal, this, &QNcursesBackend::onResize);
}

bool QNcursesBackend::present()
{
    refresh();

    return true;
}

bool QNcursesBackend::show()
{
    initscr();

    mousemask(0, NULL);

    noecho();
    cbreak();

    m_listener->start();

    onResize(QSignalHandler::_Nosig);

    return true;
}

bool QNcursesBackend::hide()
{
    def_prog_mode();
    endwin();
    m_listener->stop();
    qDebug("stopping ncyrses");

    return true;
}

bool QNcursesBackend::clearScreen()
{
    clear();

    return true;
}

QSize QNcursesBackend::size()
{
    return QSize(getmaxx(stdscr), getmaxy(stdscr));
}

void QNcursesBackend::putChar(QPoint point, quint32 ch)
{
    cchar_t t = {
            0 , // .attr
            {ch,0} // not sure how .chars works, so best guess
            };
    mvadd_wch(point.y(), point.x(), &t);
}

//NoBorder,
//LightBorder,
//DoubleBorder,
//HeavyBorder,
//LightDoubleDashBorder,
//HeavyDoubleDashBorder,
//LightTripleDashBorder,
//HeavyTripleDashBorder,
//LightQuadrupleDashBorder,
//HeavyQuadrupleDashBorder,
//LightArcBorder
uint borderMap[11][6] = {
    { //NoBorder
        ' ', //top/bottom
        ' ', //side
        ' ', //topleft
        ' ', //topright
        ' ', //bottomleft
        ' ', //bottomright
    },
    { //LightBorder
        0x2500, //top/bottom
        0x2502, //side
        0x250c, //topleft
        0x2510, //topright
        0x2514, //bottomleft
        0x2518, //bottomright
    },
    { //DoubleBorder
        0x2550, //top/bottom
        0x2551, //side
        0x2554, //topleft
        0x2557, //topright
        0x255a, //bottomleft
        0x255d, //bottomright
    },
    { //HeavyBorder
        0x2501, //top/bottom
        0x2503, //side
        0x250f, //topleft
        0x2513, //topright
        0x2517, //bottomleft
        0x251b, //bottomright
    },
    { //LightDoubleDashBorder
        0x254c, //top/bottom
        0x254e, //side
        0x250c, //topleft
        0x2510, //topright
        0x2514, //bottomleft
        0x2518, //bottomright
    },
    { //HeavyDoubleDashBorder
        0x254d, //top/bottom
        0x254f, //side
        0x250f, //topleft
        0x2513, //topright
        0x2517, //bottomleft
        0x251b, //bottomright
    },
    { //LightTripleDashBorder
        0x2504, //top/bottom
        0x2506, //side
        0x250c, //topleft
        0x2510, //topright
        0x2514, //bottomleft
        0x2518, //bottomright
    },
    { //HeavyTripleDashBorder
        0x2505, //top/bottom
        0x2507, //side
        0x250f, //topleft
        0x2513, //topright
        0x2517, //bottomleft
        0x251b, //bottomright
    },
    { //LightQuadrupleDashBorder
        0x2508, //top/bottom
        0x250a, //side
        0x250c, //topleft
        0x2510, //topright
        0x2514, //bottomleft
        0x2518, //bottomright
    },
    { //HeavyQuadrupleDashBorder
        0x2509, //top/bottom
        0x250b, //side
        0x250f, //topleft
        0x2513, //topright
        0x2517, //bottomleft
        0x251b, //bottomright
    },
    { //LightArcBorder
        0x2500, //top/bottom
        0x2502, //side
        0x256d, //topleft
        0x256e, //topright
        0x2570, //bottomleft
        0x256f, //bottomright
    },
};

void QNcursesBackend::drawBorder(QRect rect, QTerminalBorder border, QSize size)
{
    if (size.height() > 0 && rect.height() > 0 && rect.width() > 0)
        for (int i = rect.left() + 1; i < rect.right(); i++)
        {
            QPoint p(i, rect.top());
            putChar(p, borderMap[border][0]);
            if (rect.height() > 1)
            {
                p.setY(rect.bottom());
                putChar(p, borderMap[border][0]);
            }
        }
    if (size.width() > 0 && rect.width() > 0 && rect.height() > 0)
        for (int i = rect.top() + 1; i < rect.bottom(); i++)
        {
            QPoint p(rect.left(), i);
            putChar(p, borderMap[border][1]);
            if (rect.width() > 1)
            {
                p.setX(rect.right());
                putChar(p, borderMap[border][1]);
            }
        }
    if (size.width() > 0 && size.height() > 0 && rect.width() > 0 && rect.height() > 0)
    {
        putChar(rect.topLeft(), borderMap[border][2]);
        if (rect.width() > 1)
            putChar(rect.topRight(), borderMap[border][3]);
        if (rect.height() > 1)
            putChar(rect.bottomLeft(), borderMap[border][4]);
        if (rect.width() > 1 && rect.height() > 1)
            putChar(rect.bottomRight(), borderMap[border][5]);
    }

    if (size.width() > 0 || size.height() > 0)
    {
        QRect adjusted = rect.adjusted(1, 1, -1, -1);
        size.rheight()--;
        size.rwidth()--;
        drawBorder(adjusted, border, size);
    }
}

void QNcursesBackend::fillRect(QRect rect, qint32 ch)
{
    for (int y = rect.top(); y <= rect.bottom(); y++)
        for (int x = rect.left(); x <= rect.right(); x++)
        {
            QPoint p(x, y);
            putChar(p, ch);
        }
}


void QNcursesBackend::onResize(QSignalHandler::PosixSignal signal)
{
    endwin();
    refresh();
//    clear();
//    mvaddch(10, 10, '5');
    qDebug() << "Got signal" << signal << "( sigwinch:" << QSignalHandler::Sigwinch << ")" << size();
    emit resizeEvent(size());
}

void QNcursesListener::on_key(TermKey *tk, TermKeyKey *key, QHash<uint, Qt::Key> *keyMap)
{
    QTerminalKeyEvent ev;
    ev.ch = 0;
    ev.key = Qt::Key(0);
    ev.mods =
            ((TERMKEY_KEYMOD_SHIFT & key->modifiers) ? Qt::ShiftModifier : Qt::NoModifier) |
            ((TERMKEY_KEYMOD_ALT & key->modifiers) ? Qt::AltModifier : Qt::NoModifier) |
            ((TERMKEY_KEYMOD_CTRL & key->modifiers) ? Qt::ControlModifier : Qt::NoModifier);

    switch (key->type) {
    case TERMKEY_TYPE_UNICODE:
        qDebug() << "TERMKEY_TYPE_UNICODE" << key->code.codepoint << key->utf8 << key->modifiers;
        ev.ch = key->code.codepoint;
        break;
    case TERMKEY_TYPE_FUNCTION:
        qDebug() << "TERMKEY_TYPE_FUNCTION" << key->code.number << key->utf8 << key->modifiers;
        ev.key = Qt::Key(Qt::Key_F1 + key->code.number - 1);
        break;
    case TERMKEY_TYPE_KEYSYM:
        qDebug() << "TERMKEY_TYPE_KEYSYM" << key->code.sym << key->utf8 << key->modifiers;
        if (keyMap->contains(key->code.sym))
            ev.key = keyMap->value(key->code.sym);
        if (key->code.sym >= TERMKEY_SYM_KP0 && key->code.sym <= TERMKEY_SYM_KPEQUALS)
            ev.mods |= Qt::KeypadModifier;
        break;
    case TERMKEY_TYPE_MOUSE:
    {
        TermKeyMouseEvent mouseEv;
        int button, line, col;
        termkey_interpret_mouse(tk, key, &mouseEv, &button, &line, &col);
        qDebug() << "TERMKEY_TYPE_MOUSE" << mouseEv << button << line << col << key->modifiers;
        qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
        if (currentTime - lastMouseEventTime > 400 && button == lastMouseClick)
        {
//            QEvent::MouseButtonDblClick
        }
        lastMouseClick = button;
        lastMouseEventTime = currentTime;
        return;
    }
    case TERMKEY_TYPE_POSITION:
    case TERMKEY_TYPE_MODEREPORT:
    default:
        qDebug() << "other" << key->utf8 << key->modifiers;
        return;
    }

    emit keyEvent(ev);
}

QNcursesListener::QNcursesListener() :
    lastMouseEventTime(0)
{
}

void QNcursesListener::run()
{
//    QHash<uint, Qt::Key> keyMap;

//    keyMap[KEY_IC]     = Qt::Key_Insert;
//    keyMap[KEY_DC]     = Qt::Key_Delete;
////    keyMap[TERMKEY_SYM_DEL]        = Qt::Key_Backspace; // apparently
//    keyMap[KEY_HOME]       = Qt::Key_Home;
//    keyMap[KEY_END]        = Qt::Key_End;
//    keyMap[KEY_PPAGE]     = Qt::Key_PageUp;
//    keyMap[KEY_NPAGE]   = Qt::Key_PageDown;
//    keyMap[KEY_UP]         = Qt::Key_Up;
//    keyMap[KEY_DOWN]       = Qt::Key_Down;
//    keyMap[KEY_LEFT]       = Qt::Key_Left;
//    keyMap[KEY_RIGHT]      = Qt::Key_Right;

//    keyMap[TERMKEY_SYM_BACKSPACE]  = Qt::Key_Backspace;
//    keyMap[TERMKEY_SYM_ESCAPE]     = Qt::Key_Escape;
//    keyMap[TERMKEY_SYM_ENTER]      = Qt::Key_Return;
//    keyMap[TERMKEY_SYM_TAB]        = Qt::Key_Return;
//    for (int i = 0; i < 10; i++)
//        keyMap[TERMKEY_SYM_KP0 + i] = Qt::Key(Qt::Key_0 + i);
//    keyMap[TERMKEY_SYM_KPENTER]     = Qt::Key_Enter;
//    keyMap[TERMKEY_SYM_KPPLUS]      = Qt::Key_Plus;
//    keyMap[TERMKEY_SYM_KPMINUS]     = Qt::Key_Minus;
//    keyMap[TERMKEY_SYM_KPMULT]      = Qt::Key_multiply;
//    keyMap[TERMKEY_SYM_KPDIV]       = Qt::Key_division;
//    keyMap[TERMKEY_SYM_KPCOMMA]     = Qt::Key_Comma;
//    keyMap[TERMKEY_SYM_KPPERIOD]    = Qt::Key_Period;
//    keyMap[TERMKEY_SYM_KPEQUALS]    = Qt::Key_Equal;
//    int c;

//    keypad(stdscr, true);
//    MEVENT event;
//    while(1)
//    {
//        c = getch();
//        switch (c)
//        {
//        case  KEY_MOUSE:
//            getmouse(&event);
//            switch(event.bstate)
//            {
//            case BUTTON1_CLICKED:
//                qDebug() << "button1 clicked";
//                break;
//            case BUTTON1_DOUBLE_CLICKED:
//                qDebug() << "button1 dbl clicked";
//                break;
//            case BUTTON2_CLICKED:
//                qDebug() << "button2 clicked";
//                break;
//            default:
//                qDebug() << "other mouse";
//                break;
//            }
//            break;
//        default:
//            qDebug() << "key" << c;
//        }
//    }










    QHash<uint, Qt::Key> keyMap;

    keyMap[TERMKEY_SYM_INSERT]     = Qt::Key_Insert;
    keyMap[TERMKEY_SYM_DELETE]     = Qt::Key_Delete;
    keyMap[TERMKEY_SYM_DEL]        = Qt::Key_Backspace; // apparently
    keyMap[TERMKEY_SYM_HOME]       = Qt::Key_Home;
    keyMap[TERMKEY_SYM_END]        = Qt::Key_End;
    keyMap[TERMKEY_SYM_PAGEUP]     = Qt::Key_PageUp;
    keyMap[TERMKEY_SYM_PAGEDOWN]   = Qt::Key_PageDown;
    keyMap[TERMKEY_SYM_UP]         = Qt::Key_Up;
    keyMap[TERMKEY_SYM_DOWN]       = Qt::Key_Down;
    keyMap[TERMKEY_SYM_LEFT]       = Qt::Key_Left;
    keyMap[TERMKEY_SYM_RIGHT]      = Qt::Key_Right;

    keyMap[TERMKEY_SYM_BACKSPACE]  = Qt::Key_Backspace;
    keyMap[TERMKEY_SYM_ESCAPE]     = Qt::Key_Escape;
    keyMap[TERMKEY_SYM_ENTER]      = Qt::Key_Return;
    keyMap[TERMKEY_SYM_TAB]        = Qt::Key_Return;
    for (int i = 0; i < 10; i++)
        keyMap[TERMKEY_SYM_KP0 + i] = Qt::Key(Qt::Key_0 + i);
    keyMap[TERMKEY_SYM_KPENTER]     = Qt::Key_Enter;
    keyMap[TERMKEY_SYM_KPPLUS]      = Qt::Key_Plus;
    keyMap[TERMKEY_SYM_KPMINUS]     = Qt::Key_Minus;
    keyMap[TERMKEY_SYM_KPMULT]      = Qt::Key_multiply;
    keyMap[TERMKEY_SYM_KPDIV]       = Qt::Key_division;
    keyMap[TERMKEY_SYM_KPCOMMA]     = Qt::Key_Comma;
    keyMap[TERMKEY_SYM_KPPERIOD]    = Qt::Key_Period;
    keyMap[TERMKEY_SYM_KPEQUALS]    = Qt::Key_Equal;


    TERMKEY_CHECK_VERSION;

     TermKey *tk = termkey_new(0, 0);

     if(!tk) {
       fprintf(stderr, "Cannot allocate termkey instance\n");
       exit(1);
     }

     struct pollfd fd;

     fd.fd = 0; /* the file descriptor we passed to termkey_new() */
     fd.events = POLLIN;

     TermKeyResult ret;
     TermKeyKey key;

     int nextwait = -1;

     continueRun = true;

     while(continueRun) {
       if(poll(&fd, 1, nextwait) == 0) {
         // Timed out
         if(termkey_getkey_force(tk, &key) == TERMKEY_RES_KEY)
           on_key(tk, &key, &keyMap);
       }

       if(fd.revents & (POLLIN|POLLHUP|POLLERR))
         termkey_advisereadable(tk);

       while((ret = termkey_getkey(tk, &key)) == TERMKEY_RES_KEY) {
         on_key(tk, &key, &keyMap);

//         if(key.type == TERMKEY_TYPE_UNICODE &&
//            key.modifiers & TERMKEY_KEYMOD_CTRL &&
//            (key.code.codepoint == 'C' || key.code.codepoint == 'c'))
//           continueRun = false;
       }

       if(ret == TERMKEY_RES_AGAIN)
         nextwait = termkey_get_waittime(tk);
       else
         nextwait = -1;
     }

     termkey_destroy(tk);
     qDebug("stopping termkey");
}

void QNcursesListener::stop()
{
    continueRun = false;
}
