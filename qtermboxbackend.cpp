#include "qtermboxbackend.h"

#include <termbox.h>
#include <QDebug>
#include <QHash>

QTermBoxBackend::QTermBoxBackend()
{
    m_listener = new QTermBoxListener();

    connect(m_listener, &QTermBoxListener::resizeEvent, this, &QTerminalBackend::resizeEvent);
    connect(m_listener, &QTermBoxListener::keyEvent, this, &QTerminalBackend::keyEvent);
}

bool QTermBoxBackend::present()
{
    tb_present();
    return true;
}

bool QTermBoxBackend::show()
{
    m_visible = true;

    int ret = tb_init();
    switch(ret)
    {
    case TB_EUNSUPPORTED_TERMINAL:
        qDebug("TB_EUNSUPPORTED_TERMINAL");
        return false;
    case TB_EFAILED_TO_OPEN_TTY:
        qDebug("TB_EFAILED_TO_OPEN_TTY");
        return false;
    case TB_EPIPE_TRAP_ERROR:
        qDebug("TB_EPIPE_TRAP_ERROR");
        return false;
    default:
        qDebug("TermBox inited correctly");
        break;
    }

//    clear();
    m_listener->start();

    return true;
}

bool QTermBoxBackend::hide()
{
    m_visible = false;
    tb_shutdown();
    return true;
}

bool QTermBoxBackend::clearScreen()
{
    tb_clear();
    return true;
}

QSize QTermBoxBackend::size()
{
    return QSize(tb_width(), tb_height());
}

void QTermBoxBackend::putChar(QPoint point, quint32 ch)
{
    tb_cell cell;
    cell.bg = TB_WHITE;
    cell.fg = TB_RED;
    cell.ch = ch;
    tb_put_cell(point.x(), point.y(), &cell);
}

void QTermBoxListener::run()
{
    QHash<uint, uint> keyMap;

    for (int i = 0; i < 12; i++)
        keyMap[TB_KEY_F1 - i]  = Qt::Key_F1 + i;
    keyMap[TB_KEY_INSERT]      = Qt::Key_Insert;
    keyMap[TB_KEY_DELETE]      = Qt::Key_Delete;
    keyMap[TB_KEY_HOME]        = Qt::Key_Home;
    keyMap[TB_KEY_END]         = Qt::Key_End;
    keyMap[TB_KEY_PGUP]        = Qt::Key_PageUp;
    keyMap[TB_KEY_PGDN]        = Qt::Key_PageDown;
    keyMap[TB_KEY_PGDN]        = Qt::Key_PageDown;
    keyMap[TB_KEY_ARROW_UP]    = Qt::Key_Up;
    keyMap[TB_KEY_ARROW_DOWN]  = Qt::Key_Down;
    keyMap[TB_KEY_ARROW_LEFT]  = Qt::Key_Left;
    keyMap[TB_KEY_ARROW_RIGHT] = Qt::Key_Right;

    keyMap[TB_KEY_BACKSPACE]   = Qt::Key_Backspace;
    keyMap[TB_KEY_ESC]         = Qt::Key_Escape;
//    keyMap[TB_KEY_SPACE]       = Qt::Key_Space;
    keyMap[TB_KEY_ENTER]       = Qt::Key_Return;


    emit resizeEvent(QSize(tb_width(), tb_height()));

    tb_event e;

    while (tb_poll_event(&e) >= 0)
    {
        switch (e.type) {
        case TB_EVENT_KEY:
        {
            QTerminalKeyEvent k;
            k.ch = 0;
            k.key = Qt::Key(0);

            if ((e.ch > Qt::Key_Space && e.ch <= Qt::Key_QuoteLeft) ||
                (e.ch >= Qt::Key_BraceLeft && e.ch <= Qt::Key_AsciiTilde))
            {
                k.key = Qt::Key(e.ch);
                k.ch = e.ch;
            }
            else if (e.ch > Qt::Key_QuoteLeft && e.ch < Qt::Key_BraceLeft)
            {
                k.key = Qt::Key(e.ch - Qt::Key_QuoteLeft + Qt::Key_At);
                k.ch = e.ch;
            }
            else if (e.ch)
            {
                k.ch = e.ch;
            }
            else if (e.key == TB_KEY_SPACE)
            {
                k.key = Qt::Key_Space;
                k.ch = Qt::Key_Space;
            }
            else if (keyMap.contains(e.key))
            {
                k.key = Qt::Key(keyMap.value(e.key));
            }
            else
            {
                if (e.ch)
                {
                    QString hex = QString("0x%1").arg(e.ch, 0, 16);
                    qDebug() << "Key event: Char " << e.ch << hex;
                } else {
                    QString hex = QString("0x%1").arg(e.key, 0, 16);
                    qDebug() << "Key event: Key " << e.key << hex;
                }
                break;
            }
            emit keyEvent(k);
            break;
        }
        case TB_EVENT_RESIZE:
            qDebug("Got a resize");
            emit resizeEvent(QSize(e.w, e.h));
            break;
        default:
            qDebug("Got something else");
            break;
        }
    }
    qDebug("Something went wrong");

}
