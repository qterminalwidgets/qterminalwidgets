#ifndef QTERMINALWINDOW_H
#define QTERMINALWINDOW_H

#include "qterminalcontainerwidget.h"


class QTermBoxBackend;

class QTerminalWindow : public QTerminalContainerWidget
{
    Q_OBJECT
public:
    explicit QTerminalWindow(QTerminalBackend *backend = 0);
    ~QTerminalWindow();

public slots:
    void show();
    void hide();
    void setFocus(QTerminalWidget *widget);

private slots:
    void resize(const QSize &size);

    void keyEvent(const QTerminalKeyEvent &event);
    void mouseEvent(const QTerminalMouseEvent &event);

signals:

private:
    bool m_visible;

};

#endif // QTERMINALWINDOW_H
