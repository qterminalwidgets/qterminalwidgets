#ifndef QTERMINALLABEL_H
#define QTERMINALLABEL_H

#include "qterminalwidget.h"
#include <QStringList>

class QTerminalLabel : public QTerminalWidget
{
    Q_OBJECT
public:
    QTerminalLabel(QString string = QString());

    virtual QSize calculateSizeHint();
    virtual void render(bool sizeChanged);
    void recalculateSizeHints();

protected:
    QStringList wrapStrings(QStringList text, int line_length);

    QList<QTerminalWidget*> m_children;
    QStringList m_strings;
    QPoint m_displayStringStart;
};

#endif // QTERMINALLABEL_H
