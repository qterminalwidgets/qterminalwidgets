#include "qterminalcontainerwidget.h"

#include "qterminalbackend.h"

#include <QDebug>

QTerminalContainerWidget::QTerminalContainerWidget()
{
}

QTerminalContainerWidget::~QTerminalContainerWidget()
{
    foreach (const QTerminalWidget* w, m_children) {
        delete w;
    }
}

void QTerminalContainerWidget::addChild(QTerminalWidget *widget)
{
    QTerminalContainerWidget* parent = widget->parent();

    if (parent)
    {
        disconnect(widget, &QTerminalWidget::sizeHintChanged, parent, &QTerminalContainerWidget::childSizeHintChanged);
    }
    connect(widget, &QTerminalWidget::sizeHintChanged, this, &QTerminalContainerWidget::childSizeHintChanged);

    widget->setParent(this);
    widget->setBackend(m_backend);
    m_children.append(widget);
    m_dirty = true;
}

QSize QTerminalContainerWidget::calculateSizeHint() {
    QSize size;
    foreach (QTerminalWidget* w, m_children) {
        const QSize &csize = w->sizeHint();
        if (Qt::Horizontal == m_orientation)
        {
            size.rwidth() += csize.width();
            size.setHeight(qMax(size.height(), csize.height()));
        }
        else
        {
            size.rheight() += csize.height();
            size.setWidth(qMax(size.width(), csize.width()));
        }
    }

    size = checkMinMax(size);

    return size;
}

void QTerminalContainerWidget::setGeometry(const QRect &rect)
{
    QTerminalWidget::setGeometry(rect);

    QSize stuffing(m_margin.width() + m_padding.width() + m_borderSize.width(),
                   m_margin.height() + m_padding.height() + m_borderSize.height());
    QRect area = m_rect.adjusted(stuffing.width(), stuffing.height(), -stuffing.width(), -stuffing.height());

    QList<QTerminalWidget*> undecided = m_children;
    QRect childRects[m_children.size()];

    int sizeHintSum(0);
    int maxSizeSum(0);
    int minSizeSum(0);
    int availableSize(0);

    if (Qt::Horizontal == m_orientation)
        availableSize = area.width();
    else
        availableSize = area.height();

    for (int i = 0; i < undecided.size(); i++)
    {
        if (Qt::Horizontal == m_orientation)
        {
            sizeHintSum += undecided.at(i)->sizeHint().width();
            maxSizeSum += undecided.at(i)->maximumSize().width();
            minSizeSum += undecided.at(i)->minimumSize().width();
        }
        else
        {
            sizeHintSum += undecided.at(i)->sizeHint().height();
            maxSizeSum += undecided.at(i)->maximumSize().height();
            minSizeSum += undecided.at(i)->minimumSize().height();
        }
    }
    int decidedSize(0); // size of all widgets that wont grow/shrink

    for (int i = 0; i < undecided.size(); i++)
    {
        if (Qt::Horizontal == m_orientation)
        {
            if (sizeHintSum > availableSize && (undecided.at(i)->horizontalPolicy() & ShrinkFlag)) //we must shrink, keep those that can
                continue;
            if (sizeHintSum < availableSize && (undecided.at(i)->horizontalPolicy() & GrowFlag)) //we must grow, keep those that can
                continue;
            decidedSize += undecided.at(i)->sizeHint().width();
            childRects[m_children.indexOf(undecided.at(i))].setWidth(undecided.at(i)->sizeHint().width());
        }
        else
        {
            if (sizeHintSum > availableSize && (undecided.at(i)->verticalPolicy() & ShrinkFlag)) //we must shrink, keep those that can
                continue;
            if (sizeHintSum < availableSize && (undecided.at(i)->verticalPolicy() & GrowFlag)) //we must grow, keep those that can
                continue;
            decidedSize += undecided.at(i)->sizeHint().height();
            childRects[m_children.indexOf(undecided.at(i))].setHeight(undecided.at(i)->sizeHint().height());
        }
        undecided.removeAt(i); // remove all that wont grow/shrink
        i--;
    }

    int undecidedSize(availableSize - decidedSize);
    int extraSize = availableSize - (sizeHintSum - decidedSize);
    qDebug() << "undecided" << undecided.size();

    while (undecided.size() > 0)
    {
        undecidedSize = availableSize - decidedSize;
        extraSize = undecidedSize - (sizeHintSum - decidedSize); // availableSize is wrong here!!!!
//        int oddDivideRest = extraSize % undecided.size(); // gives last widget the rest
        int extraSizePerWidget[undecided.size()];
        int sumE(0);
        int sumH(0);
        for (int i = 0; i < undecided.size(); i++)
        {
            if (Qt::Horizontal == m_orientation)
            {
                extraSizePerWidget[i] = (float(extraSize - sumE) / float(sizeHintSum - decidedSize - sumH) * float(undecided.at(i)->sizeHint().width()));
                qDebug() << (float(extraSize) / float(sizeHintSum - decidedSize) * float(undecided.at(i)->sizeHint().width())) << undecided.at(i)->sizeHint().width();
                sumH += undecided.at(i)->sizeHint().width();
            }
            else
            {
                extraSizePerWidget[i] = (float(extraSize - sumE) / float(sizeHintSum - decidedSize - sumH) * float(undecided.at(i)->sizeHint().height()));
                sumH += undecided.at(i)->sizeHint().height();
            }
            sumE += extraSizePerWidget[i];
        }
//        int extraSizePerWidget = extraSize / undecided.size();

        bool restart(false);
        for (int i = 0; i < undecided.size() && !restart; i++)
        {
            if (Qt::Horizontal == m_orientation)
            {
                if (undecided.at(i)->sizeHint().width() + extraSizePerWidget[i] > undecided.at(i)->maximumSize().width() && undecided.at(i)->maximumSize().width() > 0)
                {
                    childRects[m_children.indexOf(undecided.at(i))].setWidth(undecided.at(i)->maximumSize().width());
                    decidedSize += undecided.at(i)->maximumSize().width();
                }
                else if (undecided.at(i)->sizeHint().width() + extraSizePerWidget[i] < undecided.at(i)->minimumSize().width())
                {
                    childRects[m_children.indexOf(undecided.at(i))].setWidth(undecided.at(i)->minimumSize().width());
                    decidedSize += undecided.at(i)->minimumSize().width();
                }
                else
                {
                    continue;
                }
            }
            else
            {
                if (undecided.at(i)->sizeHint().height() + extraSizePerWidget[i] > undecided.at(i)->maximumSize().height() && undecided.at(i)->maximumSize().height() > 0)
                {
                    childRects[m_children.indexOf(undecided.at(i))].setHeight(undecided.at(i)->maximumSize().height());
                    decidedSize += undecided.at(i)->maximumSize().height();
                }
                else if (undecided.at(i)->sizeHint().height() + extraSizePerWidget[i] < undecided.at(i)->minimumSize().height())
                {
                    childRects[m_children.indexOf(undecided.at(i))].setHeight(undecided.at(i)->minimumSize().height());
                    decidedSize += undecided.at(i)->minimumSize().height();
                }
                else
                {
                    continue;
                }
            }
            undecided.removeAt(i);
            restart = true;
        }
        if (restart)
            continue;

        // The ones that is still in undecided list by now gets sizeHint + extraSizePerWidget as size
        qDebug() << "All are undecided" << undecided.size() << extraSizePerWidget << decidedSize << sizeHintSum;

        for (int i = 0; i < undecided.size(); i++)
        {
            if (Qt::Horizontal == m_orientation)
                childRects[m_children.indexOf(undecided.at(i))].setWidth(undecided.at(i)->sizeHint().width() + extraSizePerWidget[i]);
            else
                childRects[m_children.indexOf(undecided.at(i))].setHeight(undecided.at(i)->sizeHint().height() + extraSizePerWidget[i]);
        }
        undecided.clear();
    }
    int currentPos(0);
    if (Qt::Horizontal == m_orientation)
        currentPos = area.left();
    else
        currentPos = area.top();

    for (int i = 0; i < m_children.size(); i++)
    {
        if (Qt::Horizontal == m_orientation)
        {
            childRects[i].setHeight(area.height());
            childRects[i].moveLeft(currentPos);
            childRects[i].moveTop(area.top());
            currentPos += childRects[i].width();
        }
        else
        {
            childRects[i].setWidth(area.width());
            childRects[i].moveTop(currentPos);
            childRects[i].moveLeft(area.left());
            currentPos += childRects[i].height();
        }

        m_children.at(i)->setGeometry(childRects[i]);


        qDebug() << "proposed size" << childRects[i];
    }
}

void QTerminalContainerWidget::render(bool)
{
}

void QTerminalContainerWidget::recalculateSizeHints()
{
    foreach (QTerminalWidget* w, m_children)
        w->recalculateSizeHints();

    m_sizeHint = calculateSizeHint();
    m_dirty = true;
}

void QTerminalContainerWidget::childSizeHintChanged()
{
    setGeometry(m_rect);
    m_backend->present();
}
